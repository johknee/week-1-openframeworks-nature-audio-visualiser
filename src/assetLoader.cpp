#include "assetLoader.h"

void AssetLoader::setup(bool _active, int _level, ofPoint _start, ofPoint _end, ofImage nImg, ofImage bgImg)
{
	start = _start;
	end = _end;
	level = _level;
	active = _active;
	set = false;
	recycle = 0;
	
	img = nImg;
	bgimg = bgImg;

	size = img.height;
	colour = ofColor(50*level,0,0);

	if(active)
	{
		active = true;
	}
	else
	{
		active = false;
	}
}
void AssetLoader::setup(bool _active)
{
	active = _active;
}

void AssetLoader::update()
{
	for(int i = 0; i < assets.size(); i++)
	{
		if(assets[i].rect.x>(end.x - img.width) - 10)
		{
			assets[i].rect.x -= 3 - (level * 0.5);
		}
	}
}

void AssetLoader::draw(ofColor curr)
{
	for(int i = 0; i < assets.size(); i++)
	{
		ofSetColor(curr);
		assets[i].bgimg->draw(assets[i].rect.x+1,assets[i].rect.y+1,bgimg.width-2,bgimg.height-2);
		ofSetColor(255);
		assets[i].img->draw(assets[i].rect.x,assets[i].rect.y);
	}
}

void AssetLoader::trigger()
{
	set = true;

	Asset curr;
	curr.img = &img;
	curr.bgimg = &bgimg;
	curr.rect.x = start.x;
	curr.rect.y = start.y - size;
	curr.rect.width = size;
	curr.rect.height = size;
	curr.colour = colour;

	if(assets.size()<25)
	{
		assets.push_back(curr);
	}
	else
	{
		assets.at(recycle) = curr;

		recycle++;

		if(recycle==25)
		{
			recycle = 0;
		}
	}
}