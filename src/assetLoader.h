#pragma once

#include "ofMain.h"

struct Asset
{
	ofRectangle rect;
	ofColor colour;
	ofImage * img;
	ofImage * bgimg;
};

class AssetLoader
{
	public:
		void setup(bool _active, int _level, ofPoint _start, ofPoint _end, ofImage nImg, ofImage bgImg);
		void setup(bool _active);
		void update();
		void draw(ofColor curr);

		void trigger();

		bool active;
		bool set;
		int level;

		float size;
		ofColor colour;
		ofImage img;
		ofImage bgimg;
		
		ofPoint start;
		ofPoint end;

		int recycle;

		vector<Asset> assets;
};