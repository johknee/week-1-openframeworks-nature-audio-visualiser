#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofEnableSmoothing();

	font.loadFont("OpenSans-Light.ttf",10.0f);
	font.setAlignment(FTGL_ALIGN_RIGHT);

	songs.push_back("awake.mp3");
	songPos = 0;
	rewindTime = 0.999;
	rewindSet = false;

	set = false;

	radius = (ofGetHeight() * 0.7) / 2;
	
	start = ofPoint((ofGetWidth()/2)+radius,ofGetHeight()/2);
	end = ofPoint((ofGetWidth()/2)-radius,ofGetHeight()/2);

	fftSmoothed = new float[8192];
	for (int i = 0; i < 8192; i++)
	{
		fftSmoothed[i] = 0;
	}

	ofSetLineWidth(27);
	
	fftBands = NUMS;
	fftTrigger = 0.2;
	
	images[0].loadImage("tree1.png");
	images[1].loadImage("tree2.png");
	images[2].loadImage("mountain.png");

	bg_images[0].loadImage("tree1_bg.png");
	bg_images[1].loadImage("tree2_bg.png");
	bg_images[2].loadImage("mountain_bg.png");

	mask.loadImage("mask.png");

	circle.loadImage("circle.png");

	for(int i = 0; i<150; i++)
	{
		star curr;
		curr.pos.x = end.x + ofRandom(radius*2);
		curr.pos.y = ((ofGetHeight()/2) - radius) + ofRandom(radius);
		curr.size = ofRandom(3,5);
		curr.speed = ofRandom(0.3);
		curr.alpha = ofRandom(255);
		stars.push_back(curr);
	}

	for(int i = 0; i < NUMS; i++)
	{
		AssetLoader curr;

		switch(i)
		{
			case 1:
				curr.setup(true,i,start,end,images[0],bg_images[0]);
				break;
			case 2:
				curr.setup(true,i,start,end,images[1],bg_images[1]);
				break;
			case 3:
				curr.setup(true,i,start,end,images[2],bg_images[2]);
				break;
			default:
				curr.setup(false);
				break;
		}

		bands[i] = curr;
	}
	
	colours[0] = ofColor(249,230,127);
	colours[1] = ofColor(211,124,153);

	mainColor = colours[0];

	ofBackground(mainColor);

	ofSetFrameRate(60);
}

//--------------------------------------------------------------
void ofApp::update(){
	ofSoundUpdate();

	float curr = song.getPosition();

	if(curr>0.999)
	{
		rewindSet = true;
		rewindTime = 0.95;
		song.setPaused(true);
	}
	
	if(rewindSet&&rewindTime<=0)
	{
		rewindTime = 0.999;
		rewindSet = false;
		nextSong();
	}

	if(rewindSet)
	{
		rewindTime -= 0.05;
		song.setPosition(rewindTime);
	}

	float * val = ofSoundGetSpectrum(fftBands);

	for (int i = 0;i < fftBands; i++)
	{
		fftSmoothed[i] *= 0.96f;

		if (fftSmoothed[i] < val[i]) fftSmoothed[i] = val[i];

		bands[i].update();
	}

	for(int i = 0; i<stars.size(); i++)
	{
		stars[i].pos.x -= stars[i].speed;

		if(stars[i].pos.x < (end.x - 10))
		{
			stars[i].pos.x = start.x + 10;
		}
	}

	mainColor = colours[0].getLerped(colours[1],song.getPosition());
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofBackground(mainColor);
	
	float sun = ofDegToRad(ofMap(song.getPosition(),0,1,180,360));

	float sunx = (ofGetWidth()/2) + (radius*0.8) * cos(sun);
	float suny = (ofGetHeight()/2) + (radius*0.8) * sin(sun);

	ofSetColor(255);
	ofEllipse(sunx,suny,50,50);

	for(int i = 0; i<stars.size(); i++)
	{
		float currAlpha = ofMap(song.getPosition(),0.7,0.9,0,stars[i].alpha,true);
		ofSetColor(ofColor(255,255,255,currAlpha));
		ofEllipse(stars[i].pos,stars[i].size,stars[i].size);
	}

	for(int i = fftBands-1;i >= 0; i--)
	{
		if(bands[i].active)
		{
			if(fftSmoothed[i]>fftTrigger)
			{
				if(!bands[i].set)
				{
					bands[i].trigger();
				}
			}
			else
			{
				bands[i].set = false;
			}

			bands[i].draw(mainColor);
		}
	}
	ofSetColor(255);
	ofRect(0,ofGetHeight()/2,ofGetWidth(),ofGetHeight());

	ofSetColor(255);
	circle.draw((ofGetWidth()/2) - radius,(ofGetHeight()/2) - radius,radius*2,radius*2);

	int amt = 6;
	float lineHeight = radius/amt;

	for(int i = 0; i < amt; i++)
	{
		drawLine((ofGetHeight()/2)+(i*lineHeight),lineHeight);
	}

	ofSetColor(mainColor);
	mask.draw(0,0,ofGetWidth(),ofGetHeight());

	float baseLine = ofGetHeight()-30;

	for(int i = 1; i<4; i++)
	{
		if(fftSmoothed[i]>fftTrigger)
		{
			ofSetColor(255,255,255,255);
		}
		else
		{
			ofSetColor(255,255,255,100);
		}

		ofRect((i*15)+15,baseLine - (fftSmoothed[i]*200),15,(fftSmoothed[i]*200));
	}

	ofSetColor(255,255,255,100);
	ofRect(30,baseLine- (fftTrigger*200),45,2);
	

	string name = prepareName(songs[songPos]);
	
	ofSetColor(255);
	font.setAlignment(FTGL_ALIGN_RIGHT);
	font.setLineLength(ofGetWidth() - 60);
	font.drawString(name,30,baseLine);

	if(!set)
	{
		ofSetColor(255);
		font.setAlignment(FTGL_ALIGN_CENTER);
		font.setLineLength(ofGetWidth() - 60);

		string text = "";
		text.append("Drag mp3s onto the window to play");
		text.append("\n");
		text.append("= and - change the threshold");

		font.drawString(text,30,30);
	}
}

void ofApp::drawLine(float x, float height)
{
	float actHeight = (float) ((height) * song.getPosition());

	actHeight = ofMap(actHeight,0,height,height*0.4,height*0.1);

	ofSetColor(mainColor);
	ofRect(0,(x+(height/2)) - (actHeight/2),ofGetWidth(),actHeight);
}

string ofApp::prepareName(string str)
{
	std::size_t found = str.find(".mp3");
	if (found!=std::string::npos)
	{
		str.erase(found,4);
	}

	found = str.find(".mp3");
	if (found!=std::string::npos)
	{
		str.erase(found,4);
	}

	unsigned founder = str.find_last_of("/\\");

	if(founder>0)
	{
		str.erase(0,founder+1);
	}

	return str;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	
	
	if(key=='-')
	{
		fftTrigger -= 0.01;
		fftTrigger = ofClamp(fftTrigger,0.05,5.0);
	}
	if(key=='=')
	{
		fftTrigger += 0.01;
		fftTrigger = ofClamp(fftTrigger,0.05,5.0);
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if(key=='s')
	{
		string filename;
		filename = "saves/"+ofToString(ofGetHours())+ofToString(ofGetMinutes())+ofToString(ofGetSeconds())+".png";
		ofSaveScreen(filename);
	}

	if(key=='f')
	{
		ofToggleFullscreen();
	};

	switch(key)
	{
		case '1':
			song.setPosition(0.1);
			break;
		case '2':
			song.setPosition(0.2);
			break;
		case '3':
			song.setPosition(0.3);
			break;
		case '4':
			song.setPosition(0.4);
			break;
		case '5':
			song.setPosition(0.5);
			break;
		case '6':
			song.setPosition(0.6);
			break;
		case '7':
			song.setPosition(0.7);
			break;
		case '8':
			song.setPosition(0.8);
			break;
		case '9':
			song.setPosition(0.9);
			break;

	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

	int newHei = (w*9)/16;
	ofSetWindowShape(w,newHei);

	radius = (ofGetHeight() * 0.7) / 2;
	
	start = ofPoint((ofGetWidth()/2)+radius,ofGetHeight()/2);
	end = ofPoint((ofGetWidth()/2)-radius,ofGetHeight()/2);

	for(int i = 0; i < NUMS; i++)
	{
		switch(i)
		{
			case 1:
			case 2:
			case 3:
				bands[i].start = start;
				bands[i].end = end;
				break;
			default:
				break;
		}
	}
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
	vector<string> curr = dragInfo.files;

	songs.clear();

	for(int i = 0;i<curr.size(); i++)
	{
		std::size_t found = curr[i].find(".mp3");
		if (found!=std::string::npos)
		{
			songs.push_back(curr[i]);
		}
	}
	songPos = 0;
	
	if(songs.size()>0)
	{
		set = true;
		playSong();
	}
}

void ofApp::nextSong()
{
	int next = songPos + 1;

	if(next>=songs.size())
	{
		songPos = 0;
	}
	else
	{
		songPos = next;
	}
	playSong();
}

void ofApp::playSong()
{
	song.loadSound(songs[songPos]);
	song.play();
	song.setPaused(false);
	song.setVolume(1);
	song.setPosition(0);
}
