#pragma once

#include "ofMain.h"
#include "assetLoader.h"
#include "ofxFTGLSimpleLayout.h"

#define NUMS 8

struct star {
	ofPoint pos;
	float size;
	float alpha;
	float speed;
};

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void drawLine(float x, float height);

		string prepareName(string str);
		
		void rewind();
		void nextSong();
		void playSong();
		
		ofSoundPlayer song;
		float * fftSmoothed;
		int fftBands;
		float fftTrigger;

		vector<string> songs;
		
		ofPoint start;
		ofPoint end;
		ofImage images[3];
		ofImage bg_images[3];
		ofImage circle;

		AssetLoader bands[NUMS];

		ofColor colours[2];

		ofImage mask;

		float radius;

		vector<star> stars;

		ofColor mainColor;

		int songPos;

		ofxFTGLSimpleLayout font;

		bool set;

		float rewindTime;
		bool rewindSet;
};
